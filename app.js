var http = require('http');
var path = require('path');
var logger = require('morgan');
var express = require('express');
var socketIo = require('socket.io');
var favicon = require('serve-favicon');
var bodyParser = require('body-parser');
var session = require('client-sessions');
var cookieParser = require('cookie-parser');

var users = require('./routes/users');
var routes = require('./routes/index');

var app = express();
var io = socketIo();
app.io = io;

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', routes);
app.use('/users', users);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
	var err = new Error('Not Found');
	err.status = 404;
	next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
	app.use(function(err, req, res, next) {
		res.status(err.status || 500);
		res.render('error', {
			message: err.message,
			error: err
		});
	});
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
	res.status(err.status || 500);
	res.render('error', {
		message: err.message,
		error: {}
	});
});

io.sockets.on('connection', function(socket) {
	socket.emit('sendMessage', 'Welcome to Scope Chat! Type !help to see available chat commands.\r\nAdditionally, you can click on the question mark in the menu bar.');
	socket.broadcast.emit('connection', 'A user connected.');

	socket.on('disconnect', function() {
		io.emit('disconnect', 'A user disconnected.');
	});

	socket.on('sendMessage', function(msg) {
		io.emit('sendMessage', msg);
	});

	socket.on('sendCommand', function(command) {
		switch (command) {
			case "!help":
				socket.emit('showHelpMenu');
			break;
			default:
				socket.emit('sendMessage', 'The command ' + command + ' has not been found.');
		}
	});
});

module.exports = app;