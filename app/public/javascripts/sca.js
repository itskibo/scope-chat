var socket = io();
$("#send-message-button").on("click tap", function(e) {
	var msg = $("#input-message").val();
	if (msg.length > 0) {
		if(msg[0] == '!') {
			socket.emit("sendCommand", msg);
		} else {
			socket.emit("sendMessage", msg);
		}
	}
});

$(function() {
	$("#input-message").on("focus", function() {
		$(this).attr("placeholder", "");
	}).on("blur", function() {
		$(this).attr("placeholder", "Type your message here");
	});
	var setMessageLayout = function(msg) {
		var date = new Date(),

			hours = date.getHours(),
			hours = (hours < 10 ? "0" : "") + hours,

			minutes = date.getMinutes(),
			minutes = (minutes < 10 ? "0" : "") + minutes,

			seconds = date.getSeconds(),
			seconds = (seconds < 10 ? "0" : "") + seconds,

			timeString = hours + ':' + minutes + ':' + seconds;

			string = timeString + ' - ' + msg;

		return string;
	}

	var addMessage = function(msg) {
		$("#messages").append($("<span class='message'>").html(setMessageLayout(msg)));
		$("#messages").scrollTop($("#messages").height());
		$("#input-message").val("");
		var msgLength = $("#messages").children().length;
		if (msgLength > 50) {
			$("#messages").find(".message:lt(50)").remove();
		}
	}

	var showHelpMenu = function() {

	}

	socket.on("sendMessage", function(msg) {
		addMessage(msg);
	});

	socket.on("connection", function(msg) {
		addMessage(msg);
	});

	socket.on("disconnect", function(msg) {
		addMessage(msg);
	});

	socket.on("sendCommand", function(command) {
		addMessage("The command " + command + " has been used.");
	});

	socket.on("showHelpMenu", function() {
		showHelpMenu();
	});
});